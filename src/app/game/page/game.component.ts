import { Nhanvat } from './../model/nhanvat.model';
import { NhanvatBiz } from './../biz-model/nhanvat.biz';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  nhavats: Nhanvat[] = [];
  show = true;
  status: boolean = false;

  constructor(private nhanvatBiz: NhanvatBiz) {}

  ngOnInit(): void {
    // this.nhanvatBiz.nhanvats$.subscribe((data) => {
    //   this.nhavats = data;
    // });
    this.nhanvatBiz.initStateMachine();
    this.nhanvatBiz.getStatus$.subscribe((e) => {
      this.status = e;
      console.log('componet-after: ' + e);
    });
    this.nhanvatBiz.getNhanvat$.subscribe((data) => {
        this.nhavats = data;
      });
    //this.nhanvatBiz.get();
  }

  abb(): void {
    this.show = this.show ? false : true;
  }

  openTrantoUpdate(ev: boolean): void {
    if (ev){
      this.nhanvatBiz.transitionToUpdate(); }
  }
  onUpdate(nv: Nhanvat): void {
    // console.log("co toi day dc ko");
    // console.log(nv);
    // console.log(nv._id);
    this.nhanvatBiz.updateNhanvat(nv, nv._id);
  }

  cancelUpdate(ev: boolean): void {
    if (ev){
      this.nhanvatBiz.cancelUpdate(); }
  }

  openTrantoDelete(ev: boolean): void {
    if (ev) {
      this.nhanvatBiz.transitionToDelete(); }
  }

  onDelete(id: string): void {
    this.nhanvatBiz.deleteNhanvat(id);
  }

  cancelDelete(ev: boolean): void{
    console.log(ev);
    if (ev) {
      this.nhanvatBiz.cancelDelete(); }
  }

  changeStatus(): void {
    const status = this.status ? false : true;
    console.log('componet: ' + status);
    //this.nhanvatBiz.changStatus(status);
  }
  createToTransition(){
    this.nhanvatBiz.transitionToCreate();
  }

  // getAbc(): Observable<string> {
  //   return of('abc'); // subcribe and unsub khi ham observable do minh tu tao
  // }
  // ngOnDestroy(){
  //   this.subcription.unsubscribe();
  // }
}
