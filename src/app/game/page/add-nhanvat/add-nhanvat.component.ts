import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NhanvatBiz } from '../../biz-model/nhanvat.biz';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-nhanvat',
  templateUrl: './add-nhanvat.component.html',
  styleUrls: ['./add-nhanvat.component.scss'],
})
export class AddNhanvatComponent implements OnInit {
  formNV: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
     private nhanvatBiz: NhanvatBiz )
     {}

  ngOnInit(): void {
    this.createForm();
    // this.nhanvatBiz.createNhanvat$.subscribe(
    //   () => {}
    //   );
  }

  createForm() {
    this.formNV = this.fb.group({
      tenServer: '',
      tenNhanVat: '',
      bang: '',
    });
  }

  add() {
    // this.nhanvatBiz.transitionToCreate();
    if (this.formNV.invalid) {
      return;
    } // valid  fe
    const nv = this.formNV.value;
    console.log(nv);
    this.nhanvatBiz.addNhanVat(nv);
  }

  cancelCreate() {
    this.nhanvatBiz.cancelCreate();
    this.router.navigateByUrl('/game');
  }
}
