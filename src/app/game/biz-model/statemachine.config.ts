import { ConfirmDelete, ConfirmUpdate, CancelDeleteRequest } from './statemachine.event';
import { Nhanvat } from './../model/nhanvat.model';
import { MachineConfig, actions } from 'xstate';
import { NhanVatStateMachineEvent, FetchSucceed, FetchFailed,
     CreateRequest, CancelCreateRequest, ConfirmCreate, CreatedSucceed, CreateFailed,
      UpdatedSucceed, UpdateFailed, DeletedSucceed, DeleteFailed, UpdateRequest, DeleteRequest } from './statemachine.event';

export interface NhanVatStateMachineSchema {
    states: {
        idle: {},
        fetching: {},
        showing: {},
        create: {},
        creating_process: {},
        update: {},
        updating_process: {},
        delete: {},
        deleting_process: {},
        errors: {}
    };
}

export interface NhanVatStateMachineContext {
    nhanvats: Nhanvat[];
}

const context: NhanVatStateMachineContext = {
    nhanvats : []
};

export const nhanvatStateMachineConfig: MachineConfig<
NhanVatStateMachineContext,
NhanVatStateMachineSchema,
NhanVatStateMachineEvent
> = {
    id : 'nhanvat State Mahine ',
    context ,
    initial : 'idle',
    states : {
        idle : {
            on : {
                '' : 'fetching'
            }
        },
        fetching: {
            invoke : {
                id : 'getAll',
                src : 'getAll'
            },
            on : {
                [FetchSucceed.type] : {
                    target : 'showing',
                    actions : ['logMessage' , 'storeData']
                },
                [FetchFailed.type] : {
                    target : 'errors',
                    actions : 'logMessage'
                }
            },
        },
        showing: {
            on : {
                [CreateRequest.type] : 'create',
                [UpdateRequest.type] : 'update',
                [DeleteRequest.type] : 'delete'
            }
        },
        create: {
            on : {
                [CancelCreateRequest.type] : 'showing',
                [ConfirmCreate.type] : 'creating_process',
            }
        },
        creating_process: {
            invoke : {
                id : 'createNhanVat',
                src : 'createNhanVat'
            },
            on : {
                [CreatedSucceed.type] : {
                    target : 'showing',
                    actions : ['logMessage' , 'createdNhanVat']
                },
                [CreateFailed.type] : {
                    target : 'showing',
                    actions : 'logMessage'
                }
            }
        },
        update: {
            on : {
                [CancelCreateRequest.type] : 'showing',
                [ConfirmUpdate.type] : 'updating_process',
            }
        },
        updating_process: {
            invoke : {
                id : 'updateNhanVat',
                src : 'updateNhanVat'
            },
            on : {
                [UpdatedSucceed.type] : {
                    target : 'showing',
                    actions : ['logMessage' , 'updatedNhanVat']
                },
                [UpdateFailed.type] : {
                    target : 'showing',
                    actions : 'logMessage'
                }
            }
        },
        delete: {
            on : {
                [CancelDeleteRequest.type] : 'showing',
                [ConfirmDelete.type] : 'deleting_process',
            }
        },
        deleting_process: {
            invoke : {
                id : 'deleteNhanVat',
                src : 'deleteNhanVat'
            },
            on : {
                [DeletedSucceed.type] : {
                    target : 'showing',
                    actions : ['logMessage' , 'deletedNhanVat']
                },
                [DeleteFailed.type] : {
                    target : 'showing',
                    actions : 'logMessage'
                }
            }
        },
        errors: {
            type : 'final'
        }
    }
}