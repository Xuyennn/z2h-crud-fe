import { NhanVatStateMachineSchema, nhanvatStateMachineConfig, NhanVatStateMachineContext } from './statemachine.config';
import { StoreNhanVat, DeleteNhanVatAction, UpdateNhanVatAction, CreateNhanVatAction } from './../state/game.action';
import { Nhanvat } from './../model/nhanvat.model';
import { Injectable } from '@angular/core';
import { GameService } from '../service/game.service';
import { BehaviorSubject, Observable, of, from } from 'rxjs';
import { first, tap, take, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Select, Store, UpdateState } from '@ngxs/store';
import { GameSelector } from '../state/game.selector';
import { ChangeStatus } from '../state/game.action';
import { NhanVatStateMachineEvent, FetchSucceed, FetchFailed,
  CreateFailed, ConfirmCreate, ConfirmUpdate,
   UpdatedSucceed, UpdateFailed, DeletedSucceed, DeleteFailed,
    ConfirmDelete, CreatedSucceed, UpdateRequest, CreateRequest, 
     CancelCreateRequest, DeleteRequest, CancelDeleteRequest } from './statemachine.event';
import { Machine, MachineOptions, interpret , assign, AnyEventObject} from 'xstate';

@Injectable()
export class NhanvatBiz {
  @Select(GameSelector.getStatus$) readonly getStatus$: Observable<boolean>;
  @Select(GameSelector.getNhanVat$) readonly getNhanvat$: Observable<Nhanvat[]>;
  // @Select(GameSelector.createNhanvat$) readonly createNhanvat$: Observable<Nhanvat>;
  // @Select(GameSelector.deelteNhanvat$) readonly deleteNV$: Observable<Nhanvat>;
  // nhanvats$ = new BehaviorSubject<Nhanvat[]>([]);
  private serviceMachine;
  private machine;
  private nhanvatStateMachineOptions: Partial<MachineOptions< NhanVatStateMachineContext , NhanVatStateMachineEvent
  >> = {
    actions : {
      logMessage : ( _ , event: AnyEventObject) => {
        console.log('%c I have a message for you: ' + event.message, 'color: blue');
      },
      storeData : ( _ , event: FetchSucceed) => {
        this.store.dispatch(new StoreNhanVat(event.nhanvats));
        return { nhanvats : event.nhanvats  };
      },
      createdNhanVat: assign( (context , event: CreatedSucceed) => {
          this.store.dispatch(new CreateNhanVatAction(event.nhanvat));
          this.router.navigateByUrl('/game');
          return {};
      }),
      updatedNhanVat : assign( (context, event: UpdatedSucceed) => {
        this.store.dispatch(new UpdateNhanVatAction(event.nhanvat));
                const nvs = [...context.nhanvats];
                const index = nvs.findIndex( e => e._id === event.nhanvat._id );
                nvs[index] = { ...event.nhanvat};
                return { nhanvats : nvs };
      }),
      deletedNhanVat : assign( (context, event: DeletedSucceed ) => {
         this.store.dispatch(new DeleteNhanVatAction(event.nhanvat));
              const nvs = [...context.nhanvats];
              const nvsFilter = nvs.filter(e => e._id !== event.nhanvat._id);
              return { nhanvats : nvsFilter };
      })
    },
    services : {
      getAll : () => (this.gameService.getData().pipe(map(
        res => {
          if (res.status === 200) {
            const msg = 'Fetched successfully!';
            return new FetchSucceed(res.body, msg);
        } else {
            const msg = 'Failed to fetch!';
            return new FetchFailed(msg);
        }
        }, catchError(() => {
        const msg = 'Failed to fetch!';
        return of(new FetchFailed(msg));
        }
      )))),
        createNhanVat : ( _ , event: ConfirmCreate) => (
        this.gameService.createNhanVat(event.nhanvat).pipe(
            map(res => {
                if (res.status === 201) {
                    const msg = 'Created succesfully!';
                    return new CreatedSucceed(res.body, msg);
                } else {
                    const msg = 'Failed to create!';
                    return new CreateFailed(msg);
                }
            }, catchError(() => {
                const msg = 'Failed to create!';
                return of(new CreateFailed(msg));
            }))
        )),
      updateNhanVat : (_, event: ConfirmUpdate) => (
          this.gameService.updateNhanVat(event.id , event.nhanvat).pipe(
            map( res => {
              if (res.status === 200 ){
                const msg = 'Update succesfully!';
                return new UpdatedSucceed(res.body, msg);
              } else {
                const msg = 'Failed to update!';
                return new UpdateFailed(msg);
              }
            } , catchError(() => {
              const msg = 'Failed to update!';
              return of(new UpdateFailed(msg));
            })
          )
        )
      ),
      deleteNhanVat : (_, event: ConfirmDelete) => (
        this.gameService.deleteNhanVat(event.id).pipe(
            map(res => {
                if (res.status === 200) {
                    const msg = 'Deleted successfully!';
                    //this.store.dispatch(new DeleteNhanVatAction(res.body));
                    return new DeletedSucceed(res.body, msg);
                } else {
                    const msg = 'Failed to delete!';
                    return new DeleteFailed(msg);
                }
            }, catchError(() => {
                const msg = 'Failed to delete!';
                return of(new DeleteFailed(msg));
            }))
        )
    )
    }
  };

  constructor(
    private gameService: GameService,
    private router: Router,
    private store: Store
  ) {}

  initStateMachine(): void {
    this.machine = Machine<any , NhanVatStateMachineSchema,NhanVatStateMachineEvent>(nhanvatStateMachineConfig)
                  .withConfig(this.nhanvatStateMachineOptions);
    this.serviceMachine = interpret(this.machine).start();
    const state$ = from(this.serviceMachine);
        state$.subscribe((state: any) => {
    //         console.log(`---------------------`);
    //         console.log(`State changed the ${this.cnt++} time!`);
    //         if (!!state.transitions[0]) {
    //             console.log(`Event: ${state.transitions[0].event} has just been sent!`);
    //         }
            console.log(`Current state: ${state.value}`);
    //         console.log(`Available events: ${state.nextEvents}`);
    //         console.log(state);
    //         console.log(`---------------------`);
        });

  }
  stopMachine(): void {
    this.serviceMachine.stop();
  }

  transition(event: NhanVatStateMachineEvent) {
      this.serviceMachine.send(event);
  }

  transitionToCreate(): void {
      this.transition(new CreateRequest());
  }

  addNhanVat(nv: Nhanvat): void {
    this.transition(new ConfirmCreate(nv));
  }

  cancelCreate(): void {
    this.transition( new CancelCreateRequest());
  }


  transitionToUpdate(): void {
    this.transition(new UpdateRequest());
  }
  updateNhanvat(nv: Nhanvat, id: string): void {
    this.transition(new ConfirmUpdate(nv, id));
  }

  cancelUpdate(): void {
    this.transition(new CancelCreateRequest());
  }

  transitionToDelete(): void {
    this.transition(new DeleteRequest());
  }
  deleteNhanvat(id: string): void {
    this.transition(new ConfirmDelete(id));
  }

  cancelDelete(): void {
    this.transition(new CancelDeleteRequest());
  }
  // get() {
  //   this.gameService.getData().pipe(map(res => {
  //     this.store.dispatch(new StoreNhanVat(res));
  //   });
  // }

  // add(nv: Nhanvat) {
  //   this.gameService.createNhanVat(nv).subscribe(
  //     (res) => {
  //       this.router.navigateByUrl('/game');
  //       //this.store.dispatch(new CreateNhanVatAction(res));
  //     },
  //     () => console.log('failed')
  //   );
  // }

  // update(id: string, nv: Nhanvat) {
  //   this.gameService.updateNhanVat(id, nv).subscribe(
  //     (res) => {
  //       this.store.dispatch( new UpdateNhanVatAction(res));
  //     }
  //   );
  // }

  // delete(id: string) {
  //   this.gameService.deleteNhanVat(id).subscribe(
  //     // () => {
  //     //   this.nhanvats$.pipe(first()).subscribe((list) => {
  //     //     const filteredList = list.filter((ele) => ele._id !== id);
  //     //     this.nhanvats$.next([...filteredList]);
  //     //   });
  //     // },
  //     // () => console.log('failed')
  //     // (res) => {
  //     //   this.store.dispatch( new DeleteNhanVatAction(res));
  //     // }
  //     (res) => {
  //       this.store.dispatch(new DeleteNhanVatAction( res )) ;
  //      }
  //     //() => this.get()
  //     );
  // }

  // changStatus(status: boolean) {
  //   console.log('biz: ' + status);
  //   this.store.dispatch(new ChangeStatus(status));
  // }
}
