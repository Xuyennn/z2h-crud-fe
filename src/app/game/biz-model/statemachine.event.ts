import { Nhanvat } from './../model/nhanvat.model';
import { EventObject } from 'xstate';

export class FetchSucceed implements EventObject {
    static readonly type = '[Nhanvat] FETCH SUCCEED';
    readonly type = FetchSucceed.type;
    constructor(
        public nhanvats: Nhanvat[],
        public message: string
    ) { }
}
export class FetchFailed implements EventObject {
    static readonly type = '[Nhanvat] FETCH Failed';
    readonly type = FetchFailed.type;
    constructor(
        public message: string
    ) { }
}
export class CreateRequest implements EventObject {
    static readonly type = '[Nhanvat] Create request';
    readonly type = CreateRequest.type;
    constructor(
    ) { }
}
export class CancelCreateRequest implements EventObject {
    static readonly type = '[Nhanvat] Cancel Create request';
    readonly type = CancelCreateRequest.type;
    constructor(
    ) { }
}
export class ConfirmCreate implements EventObject {
    static readonly type = '[Nhanvat] Confirm Create';
    readonly type = ConfirmCreate.type;
    constructor( public nhanvat: Nhanvat
    ) { }
}
export class CreatedSucceed implements EventObject {
    static readonly type = '[Nhanvat]  Create Succeed';
    readonly type = CreatedSucceed.type;
    constructor(
        public nhanvat: Nhanvat ,
        public message: string
    ) { }
}
export class CreateFailed implements EventObject {
    static readonly type = '[Nhanvat] Create Failed ';
    readonly type = CreateFailed.type;
    constructor(
        public message: string
    ) { }
}
export class UpdateRequest implements EventObject {
    static readonly type = '[Nhanvat] Update request';
    readonly type = UpdateRequest.type;
    constructor(
    ) { }
}
export class CancelUpdateRequest implements EventObject {
    static readonly type = '[Nhanvat] Cancel Update request';
    readonly type = CancelUpdateRequest.type;
    constructor(
    ) { }
}
export class ConfirmUpdate implements EventObject {
    static readonly type = '[Nhanvat] Confirm Update';
    readonly type = ConfirmUpdate.type;
    constructor( public nhanvat: Nhanvat ,
                 public id: string
    ) { }
}
export class UpdatedSucceed implements EventObject {
    static readonly type = '[Nhanvat]  Update Succeed';
    readonly type = UpdatedSucceed.type;
    constructor( public nhanvat: Nhanvat ,
                 public message: string
    ) { }
}
export class UpdateFailed implements EventObject {
    static readonly type = '[Nhanvat] Update Failed ';
    readonly type = UpdateFailed.type;
    constructor( public message: string
    ) { }
}
export class DeleteRequest implements EventObject {
    static readonly type = '[Nhanvat] Delete request';
    readonly type = DeleteRequest.type;
    constructor(
    ) { }
}
export class CancelDeleteRequest implements EventObject {
    static readonly type = '[Nhanvat] Cancel Delete request';
    readonly type = CancelDeleteRequest.type;
    constructor(
    ) { }
}
export class ConfirmDelete implements EventObject {
    static readonly type = '[Nhanvat] Confirm Delete';
    readonly type = ConfirmDelete.type;
    constructor( public id: string
    ) { }
}
export class DeletedSucceed implements EventObject {
    static readonly type = '[Nhanvat]  Deleted Succeed';
    readonly type = DeletedSucceed.type;
    constructor(
        public nhanvat: Nhanvat,
        public message: string
    ) { }
}
export class DeleteFailed implements EventObject {
    static readonly type = '[Nhanvat] Delete Failed ';
    readonly type = DeleteFailed.type;
    constructor(
        public message: string
    ) { }
}

export type NhanVatStateMachineEvent = FetchSucceed | FetchFailed
                                | CreateRequest | ConfirmCreate | CancelCreateRequest | CreatedSucceed | CreateFailed
                                | UpdateRequest | ConfirmUpdate | CancelUpdateRequest | UpdatedSucceed |  UpdateFailed
                                | DeleteRequest | ConfirmDelete | CancelDeleteRequest | DeletedSucceed | DeleteFailed;