import { Nhanvat } from './../../model/nhanvat.model';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  TemplateRef,
} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() nhavats: Nhanvat[] = [];

  @Output() trantoUpd: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() upNhanvat: EventEmitter<Nhanvat> = new EventEmitter<Nhanvat>();
  @Output() cancelUp: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() trantoDel: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() nhanvenId: EventEmitter<string> = new EventEmitter<string>();
  @Output() cancelDele: EventEmitter<boolean> = new EventEmitter<boolean>();

  modalRef: BsModalRef;
  editingRow = '';
  isHidden = true;
  nvOdd: Nhanvat;

  constructor(private modalService: BsModalService) {}

  ngOnInit(): void {}
  openUpdate(id: string): void {
      this.editingRow = id;
      this.isHidden = false;
      this.nvOdd = { ...this.nhavats.find((e) => e._id === id) };
      this.trantoUpd.emit(true);
  }

  onUpdate(nv: Nhanvat): void {
    console.log(nv);
    this.upNhanvat.emit(nv);
    this.isHidden = true;
    this.editingRow = '';
  }
  cancelUpdate(id: string): void {
    this.editingRow = '';
    this.isHidden = true;
    const index = this.nhavats.indexOf(this.nhavats.find((e) => e._id === id));
    this.nhavats[index] = this.nvOdd;
    this.cancelUp.emit(true);
  }

  openModal(template: TemplateRef<any>): void {
    this.trantoDel.emit(true);
    this.modalRef = this.modalService.show(template);
  }

  onDelete(id: string): void {
    console.log('delete');
    this.nhanvenId.emit(id);
    this.modalRef.hide();
    this.isHidden = true;
  }
  cancelDelete(): void {
    this.modalRef.hide();
    this.cancelDele.emit(true);
  }
}
