import { environment } from './../../../environments/environment';
import { Nhanvat } from './../model/nhanvat.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const baseUrl = environment.baseUrl ;

@Injectable()
export class GameService {
  constructor(private http: HttpClient) {}

  getData(): Observable<HttpResponse<Nhanvat[]>> {
    return this.http.get<Nhanvat[]>(baseUrl + '/game' , {
      observe: 'response'
    }).pipe(
      map(res => res),
      catchError(err => of(err))
  );
  }

  createNhanVat(nv: Nhanvat): Observable<HttpResponse<Nhanvat>> {
    return this.http.post<Nhanvat>(baseUrl + '/game', nv, {
      observe: 'response'
    }).pipe(
      map(res => res),
      catchError(err => of(err))
  );
  }

  updateNhanVat(id: string , nv: Nhanvat): Observable<HttpResponse<Nhanvat>>{
   return this.http.put<Nhanvat>(baseUrl + '/game/' + id, nv , {
    observe: 'response'
   }).pipe(
    map(res => res),
    catchError(err => of(err))
);
  }

  deleteNhanVat(id: string): Observable<HttpResponse<Nhanvat>> {
    return this.http.delete<Nhanvat>(baseUrl + '/game/' + id , {
      observe: 'response'
    });
  }
}
