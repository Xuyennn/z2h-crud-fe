import { GameSelector } from './state/game.selector';
import { AddNhanvatComponent } from './page/add-nhanvat/add-nhanvat.component';
import { GameComponent } from './page/game.component';
import { NgModule } from '@angular/core';
import { GameRoutingModule } from './game-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { GameService } from './service/game.service';
import { CommonModule } from '@angular/common';
import { NhanvatBiz } from './biz-model/nhanvat.biz';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './component/table/table.component';
import { NgxsModule } from '@ngxs/store';
import { GameState } from './state/game.state';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [GameComponent, AddNhanvatComponent, TableComponent],
  imports: [
    GameRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forFeature([GameState]),
    ModalModule.forChild()
  ],
  providers: [
    GameService,
     NhanvatBiz,
     GameSelector
    ],
})
export class GameModule {}
