import { GameComponent } from './page/game.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNhanvatComponent } from './page/add-nhanvat/add-nhanvat.component';


const routes: Routes = [
  {
    path : '',
    component : GameComponent
  },
  {
    path : 'add',
    component : AddNhanvatComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
