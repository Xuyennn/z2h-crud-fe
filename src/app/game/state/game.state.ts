import { Nhanvat } from './../model/nhanvat.model';
import { State, Action, StateContext } from '@ngxs/store';
import { ChangeStatus, StoreNhanVat, DeleteNhanVatAction } from './game.action';

export interface GameStateModel {
  status: boolean;
  nhanVats : Nhanvat[];
  nhanvat : Nhanvat ;
}

@State<GameStateModel>({
  name: 'GameState',
  defaults: {
    status: true,
    nhanVats : [
        { _id:'a91449f9-d27d-42f2-9f8b-140529021515', tenNhanVat : 'xu', tenServer : 'xu', bang: 'xu'},
        { _id:'a91449f9-d27d-42f2-9f8b-140529024515', tenNhanVat : 'xu', tenServer : 'xu', bang: 'xu'}
    ],
    nhanvat : null
  },
})
export class GameState {
  @Action(ChangeStatus)
  changeStatus(ctx: StateContext<GameStateModel>, action: ChangeStatus) {
    console.log('state: ' + action.payload);
    ctx.patchState({
      status: action.payload,
    });
  }

  @Action(StoreNhanVat)
  storeNhanVat(ctx: StateContext<GameStateModel>, action: StoreNhanVat) {
    console.log('state: ' + action.payload);
    ctx.patchState({
      nhanVats : action.payload,
    });
  }

  // @Action(CreateNhanVatAction)
  // createNhanVatAction(ctx: StateContext<GameStateModel>, action: CreateNhanVatAction) {
  //   console.log('state: ' + action.payload);
  //   ctx.patchState({
  //     nhanvat : action.payload,
  //   });
  // }

  @Action(DeleteNhanVatAction)
  deleteNhanVatAction(ctx: StateContext<GameStateModel>, action: DeleteNhanVatAction) {
    const list = ctx.getState().nhanVats;
    const filteredList = list.filter(nv => nv._id !== action.payload._id);
    ctx.patchState({
      nhanVats: [ ...filteredList ]
    });
  }

  @Action(DeleteNhanVatAction)
  updateNhanVatAction(ctx: StateContext<GameStateModel>, action: DeleteNhanVatAction) {
    const list = ctx.getState().nhanVats;
    const index = list.indexOf( list.find(e => e._id === action.payload._id ) );
    list[index] = action.payload;
    ctx.patchState({
      nhanVats: [ ...list ]
    });
  }
}
