import { GameState, GameStateModel } from './game.state';
import { Injectable } from '@angular/core';
import { Selector } from '@ngxs/store';

//@Injectable() // tai sao cho nay ko can inject van dc ????
export class GameSelector {
  @Selector([GameState])
  static getStatus$(state: GameStateModel) {
    console.log('selector: ' + state.status);
    return state.status;
  }
  @Selector([GameState])
  static getNhanVat$(state: GameStateModel) {
    //console.log('selector: ' + state.status);
    return state.nhanVats;
  }
  // @Selector([GameState])
  // static createNhanvat$(state: GameStateModel) {
  //   //console.log('selector: ' + state.status);
  //   return state.nhanvat;
  // }
  // @Selector([GameState])
  // static deelteNhanvat$(state: GameStateModel) {
  //   //console.log('selector: ' + state.status);
  //   return state.nhanvat;
  // }
}
