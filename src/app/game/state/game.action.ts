import { Nhanvat } from './../model/nhanvat.model';
export class ChangeStatus {
    static readonly type = '[GAME] Change status';
    constructor( public payload : boolean){
    }
}

export class StoreNhanVat {
    static readonly type = '[GAME] Store nhanvat';
    constructor( public payload: Nhanvat[]){
    }
}

export class CreateNhanVatAction {
    static readonly type = '[GAME] Delete  nhanvat';
    constructor( public payload: Nhanvat){
    }
}

export class DeleteNhanVatAction {
    static readonly type = '[GAME] Delete nhanvat';
    constructor( public payload: Nhanvat){
    }
}

export class UpdateNhanVatAction {
    static readonly type = '[GAME] update nhanvat';
    constructor( public payload: Nhanvat){
    }
}